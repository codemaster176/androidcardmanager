package com.kongent.cardtest;

import android.database.sqlite.SQLiteConstraintException;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.kongent.cardtest.model.AadharCard;
import com.kongent.cardtest.model.BaseCard;
import com.kongent.cardtest.model.PanCard;

public class ActivityAddCard extends AppCompatActivity implements View.OnClickListener {
    private int cardType;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_card);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setSupportActionBar(toolbar);
        cardType = getIntent().getExtras().getInt(showoradd.CARD_TYPE);
        userID = getIntent().getExtras().getString(showoradd.ARG_USER_ID);
        setupView();
    }

    private void setupView() {
        switch (cardType) {
            case showoradd.ARG_CARD_TYPE_AADHAR: {
                getSupportActionBar().setTitle("Add Aadhar Card");
                findViewById(R.id.view_aadhar).setVisibility(View.VISIBLE);
                findViewById(R.id.btn_save_aadhar).setOnClickListener(this);
                break;
            }
            case showoradd.ARG_CARD_TYPE_DEBIT: {
                getSupportActionBar().setTitle("Add Debit Card");
            }
            case showoradd.ARG_CARD_TYPE_CREDIT:{
                getSupportActionBar().setTitle("Add Credit Card");
                findViewById(R.id.view_debit_or_credit).setVisibility(View.VISIBLE);
                findViewById(R.id.btn_save_base_card).setOnClickListener(this);
                break;
            }
            case showoradd.ARG_CARD_TYPE_PAN: {
                getSupportActionBar().setTitle("Add Pan Card");
                findViewById(R.id.view_pan).setVisibility(View.VISIBLE);
                findViewById(R.id.btn_save_pan_card).setOnClickListener(this);
                break;
            }
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_save_aadhar: {
                handleSaveAadhar();
                break;
            }
            case R.id.btn_save_base_card: {
                handleSaveBaseCard();
                break;
            }
            case R.id.btn_save_pan_card:{
                handleSavePanCard();
                break;
            }
        }
    }

    private void handleSaveAadhar() {
        EditText etFullName = (EditText)findViewById(R.id.et_aadhar_full_name);
        EditText etNumber = (EditText)findViewById(R.id.et_aadhar_number);
        EditText etAddress = (EditText)findViewById(R.id.et_aadhar_address);
        boolean hasError = false;
        if (TextUtils.isEmpty(etFullName.getText().toString())) {
            etFullName.setError("Full namem is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(etNumber.getText().toString())) {
            etNumber.setError("Aadhar number is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(etAddress.getText().toString())) {
            etAddress.setError("Address is required.");
            hasError = true;
        }
        if (!hasError) {
            DBHelper dbHelper = new DBHelper(this);
            try {
                dbHelper.saveAadharCard(new AadharCard(userID, etFullName.getText().toString(), etAddress.getText().toString(), etNumber.getText().toString()));
                Toast.makeText(this, "Card successfully saved", Toast.LENGTH_SHORT).show();
                finish();
            } catch (SQLiteConstraintException ex) {
                Snackbar.make(findViewById(android.R.id.content), "Card with that number already exists.", Snackbar.LENGTH_SHORT);
            }
        }
    }

    private void handleSaveBaseCard() {
        EditText etCCV = (EditText)findViewById(R.id.et_base_card_ccv);
        EditText etExpiryDate = (EditText)findViewById(R.id.et_base_card_expiry_date);
        EditText etFullName = (EditText)findViewById(R.id.et_base_card_name);
        EditText etNumber = (EditText)findViewById(R.id.et_base_card_number);
        boolean hasError = false;
        if (TextUtils.isEmpty(etCCV.getText().toString())) {
            etCCV.setError("CCV is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(etExpiryDate.getText().toString())) {
            etExpiryDate.setError("Expiry Date is Required");
            hasError = true;
        }
        if (TextUtils.isEmpty(etFullName.getText().toString())) {
            etFullName.setError("Full Name is required");
            hasError = true;
        }
        if (TextUtils.isEmpty(etNumber.getText().toString())) {
            etNumber.setError("Card Number is required.");
            hasError = true;
        }
        if (!hasError) {
            DBHelper db = new DBHelper(this);
            String tableName = cardType == showoradd.ARG_CARD_TYPE_CREDIT ? DBContract.CreditCard.TABLE_NAME : DBContract.DebitCard.TABLE_NAME;
            try {
                db.saveBaseCard(new BaseCard(userID, etFullName.getText().toString(), etCCV.getText().toString(), etExpiryDate.getText().toString(), etNumber.getText().toString()), tableName);
                Toast.makeText(this, "Card successfully saved", Toast.LENGTH_SHORT).show();
                finish();
            } catch (SQLiteConstraintException ex) {
                Snackbar.make(findViewById(android.R.id.content), "Card with that number already exists.", Snackbar.LENGTH_SHORT);
            }
        }
    }

    private void handleSavePanCard() {
        EditText etFullName = (EditText)findViewById(R.id.et_pan_full_name);
        EditText etDOB = (EditText)findViewById(R.id.et_pan_dob);
        EditText etPanNum = (EditText)findViewById(R.id.et_pan_number);
        boolean hasError = false;
        if (TextUtils.isEmpty(etFullName.getText().toString())) {
            etFullName.setError("Full name is required");
            hasError = true;
        }
        if (TextUtils.isEmpty(etDOB.getText().toString())) {
            etDOB.setError("Date of Birth is required");
            hasError = true;
        }
        if (TextUtils.isEmpty(etPanNum.getText().toString())) {
            etPanNum.setError("PAN Number is required");
            hasError = true;
        }
        if (!hasError) {
            DBHelper db = new DBHelper(this);
            try {
                db.savePanCard(new PanCard(userID, etFullName.getText().toString(), etDOB.getText().toString(), etPanNum.getText().toString()));
                Toast.makeText(this, "Card successfully saved", Toast.LENGTH_SHORT).show();
                finish();
            } catch (SQLiteConstraintException ex) {
                Snackbar.make(findViewById(android.R.id.content), "Card with that number already exists.", Snackbar.LENGTH_SHORT);
            }
        }
    }
}
