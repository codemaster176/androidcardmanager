package com.kongent.cardtest;

import android.provider.BaseColumns;


public final class DBContract {
    public static class User implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_EMAIL = "email";
        public static final String COLUMN_LAST_NAME = "last_name";
        public static final String COLUMN_FIRST_NAME = "first_name";
        public static final String COLUMN_PASSWORD = "password";
        public static final String[] ALL_COLUMNS = new String[]{
                COLUMN_EMAIL,
                COLUMN_PASSWORD,
                COLUMN_LAST_NAME,
                COLUMN_FIRST_NAME,
                _ID
        };
    }

    public static class CreditCard extends BaseCard {
        public static final String TABLE_NAME = "credit_card";
    }

    public static class DebitCard extends BaseCard {
        public static final String TABLE_NAME = "debit_card";
    }

    public static class BaseCard implements BaseColumns {
        public static final String COLUMN_CVV = "cvv";
        public static final String COLUMN_CARD_NUMBER = "card_number";
        public static final String COLUMN_CARD_NAME = "card_name";
        public static final String COLUMN_EXPIRY_DATE = "expiry_date";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String[] ALL_COLUMNS = new String[] {
                _ID,
                COLUMN_CARD_NAME,
                COLUMN_CARD_NUMBER,
                COLUMN_CVV,
                COLUMN_EXPIRY_DATE,
                COLUMN_USER_ID
        };
    }

    public static class PanCard implements BaseColumns {
        public static final String TABLE_NAME = "pan_card";
        public static final String COLUMN_PAN_NUMBER = "pan_number";
        public static final String COLUMN_DOB = "dob";
        public static final String COLUMN_NAME = "name";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String[] ALL_COLUMNS = new String[]{
                _ID,
                COLUMN_DOB,
                COLUMN_NAME,
                COLUMN_PAN_NUMBER,
                COLUMN_USER_ID
        };
    }

    public static class AadharCard implements BaseColumns {
        public static final String TABLE_NAME = "aadhar_card";
        public static final String COLUMN_AADHAR_NUMBER = "aadhar_number";
        public static final String COLUMN_FULL_NAME = "full_name";
        public static final String COLUMN_ADDRESS = "address";
        public static final String COLUMN_USER_ID = "user_id";
        public static final String[] ALL_COLUMNS = new String[]{
                _ID,
                COLUMN_AADHAR_NUMBER,
                COLUMN_ADDRESS,
                COLUMN_FULL_NAME,
                COLUMN_USER_ID
        };
    }
}
