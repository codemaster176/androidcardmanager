package com.kongent.cardtest.model;

/**
 * Created by Arun Kumar on 14/01/2018.
 */

public class User {
    public String firstName;
    public String email;
    public String lastName;
    public String password;
    public String id;
}
