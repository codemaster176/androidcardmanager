package com.kongent.cardtest.model;

/**
 * Created by Arun Kumar on 14/01/2018.
 */

public class BaseCard {
    public String userId;
    public String ccv;
    public String expiryDate;
    public String cardNum;
    public String fullName;

    public BaseCard(String userId, String fullName, String ccv, String expiryDate, String cardNum) {
        super();
        this.userId = userId;
        this.fullName = fullName;
        this.cardNum = cardNum;
        this.expiryDate = expiryDate;
        this.ccv = ccv;
    }
}
