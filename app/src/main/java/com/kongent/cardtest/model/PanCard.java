package com.kongent.cardtest.model;

/**
 * Created by Arun Kumar on 14/01/2018.
 */

public class PanCard {
    public String fullName;
    public String dob;
    public String panNumber;
    public String userId;

    public PanCard(String uID, String fName, String d, String pNum) {
        super();
        this.userId = uID;
        this.fullName = fName;
        this.dob = d;
        this.panNumber = pNum;
    }
}
