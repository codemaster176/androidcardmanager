package com.kongent.cardtest.model;

/**
 * Created by Arun Kumar on 14/01/2018.
 */

public class AadharCard {
    public String userID;
    public String fullName;
    public String address;
    public String aadharNum;

    public AadharCard(String userID, String fullName, String address, String aadharNum) {
        super();
        this.userID = userID;
        this.fullName = fullName;
        this.address = address;
        this.aadharNum = aadharNum;
    }
}
