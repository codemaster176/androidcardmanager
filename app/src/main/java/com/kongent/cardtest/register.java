package com.kongent.cardtest;

import android.content.Intent;
import android.database.sqlite.SQLiteConstraintException;
import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.test.suitebuilder.annotation.SmallTest;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.kongent.cardtest.model.User;

import org.w3c.dom.Text;

public class register extends AppCompatActivity {
    private EditText etFirstName, etLastName, etPassword, etConfirmPassword, etEmail;
    private Button btnRegister;
    private Snackbar snackbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        etFirstName = (EditText)findViewById(R.id.et_first_name);
        etLastName = (EditText)findViewById(R.id.et_last_name);
        etPassword = (EditText)findViewById(R.id.et_password);
        etConfirmPassword = (EditText)findViewById(R.id.et_confirm_password);
        etEmail = (EditText)findViewById(R.id.et_email);
        btnRegister = (Button)findViewById(R.id.btn_register);
    }

    public void register(View view) {
        User user = new User();
        user.firstName = etFirstName.getText().toString();
        user.lastName = etLastName.getText().toString();
        user.password = etPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        user.email = etEmail.getText().toString();
        boolean hasError = false;
        if (TextUtils.isEmpty(user.firstName)) {
            etFirstName.setError("First name is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(user.lastName)) {
            etLastName.setError("Last name is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(user.password)){
            etPassword.setError("Password is required.");
            hasError = true;
        }
        if (TextUtils.isEmpty(confirmPassword)) {
            etConfirmPassword.setError("Confirm your password.");
            hasError = true;
        }
        if (TextUtils.isEmpty(user.email)) {
            etEmail.setError("Email is required.");
            hasError = true;
        }
        if (!user.password.equals(confirmPassword)) {
            etConfirmPassword.setError("Password does not match.");
            hasError = true;
        }

        if (!hasError) {
            new TaskRegister().execute(user);
        }
    }

    private class TaskRegister extends AsyncTask<User, Void, Boolean> {
        private String errorMsg = "Unknown error occured. Please Try again.";
        @Override
        protected void onPreExecute() {
            btnRegister.setEnabled(false);
            snackbar = Snackbar.make(findViewById(android.R.id.content), "Registering user, please wait", Snackbar.LENGTH_INDEFINITE);
            snackbar.show();
        }

        @Override
        protected Boolean doInBackground(User[] users) {
            DBHelper db = new DBHelper(getApplicationContext());
            try {
                return db.saveUser(users[0]);
            } catch(SQLiteConstraintException ex) {
                errorMsg = users[0].email + " is already taken. Try another email.";
                return  false;
            }
        }

        @Override
        protected void onPostExecute(Boolean saved) {
            btnRegister.setEnabled(true);
            snackbar.dismiss();
            if (saved) {
                Snackbar.make(findViewById(android.R.id.content), "Please log in to continue.", Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(getApplicationContext(), login.class);
                startActivity(intent);
            } else {
                Snackbar.make(findViewById(android.R.id.content), errorMsg, Snackbar.LENGTH_SHORT).show();
            }
        }
    }
}
