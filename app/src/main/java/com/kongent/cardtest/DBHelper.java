package com.kongent.cardtest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.kongent.cardtest.model.AadharCard;
import com.kongent.cardtest.model.BaseCard;
import com.kongent.cardtest.model.PanCard;
import com.kongent.cardtest.model.User;

import java.util.ArrayList;

/**
 * Created by Arun Kumar on 2016.
 */

public class DBHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "app.db";
    public static final String SQL_CREATE_USER_TABLE = "CREATE TABLE " + DBContract.User.TABLE_NAME +
            " (" + DBContract.User._ID + " INTEGER PRIMARY KEY," +
            DBContract.User.COLUMN_EMAIL + " TEXT UNIQUE, " +
            DBContract.User.COLUMN_FIRST_NAME + " TEXT, " +
            DBContract.User.COLUMN_LAST_NAME + " TEXT, " +
            DBContract.User.COLUMN_PASSWORD + " TEXT)";

    public static final String SQL_CREATE_PAN_CARD_TABLE = "CREATE TABLE " + DBContract.PanCard.TABLE_NAME +
            " (" + DBContract.PanCard._ID + " INTEGER PRIMARY KEY," +
            DBContract.PanCard.COLUMN_PAN_NUMBER + " TEXT UNIQUE, " +
            DBContract.PanCard.COLUMN_NAME + " TEXT, " +
            DBContract.PanCard.COLUMN_DOB + " TEXT, " +
            DBContract.PanCard.COLUMN_USER_ID + " TEXT)";

    public static final String SQL_CREATE_AADHAR_CARD_TABLE = "CREATE TABLE " + DBContract.AadharCard.TABLE_NAME +
            " (" + DBContract.AadharCard._ID + " INTEGER PRIMARY KEY," +
            DBContract.AadharCard.COLUMN_AADHAR_NUMBER + " TEXT UNIQUE, " +
            DBContract.AadharCard.COLUMN_USER_ID + " TEXT, " +
            DBContract.AadharCard.COLUMN_ADDRESS + " TEXT, " +
            DBContract.AadharCard.COLUMN_FULL_NAME + " TEXT)";

    public static final String SQL_CREATE_CREDIT_CARD_TABLE = "CREATE TABLE " + DBContract.CreditCard.TABLE_NAME +
            " (" + DBContract.CreditCard._ID + " INTEGER PRIMARY KEY," +
            DBContract.CreditCard.COLUMN_CARD_NUMBER + " TEXT UNIQUE, " +
            DBContract.CreditCard.COLUMN_CARD_NAME + " TEXT, " +
            DBContract.CreditCard.COLUMN_CVV + " TEXT, " +
            DBContract.CreditCard.COLUMN_EXPIRY_DATE + " TEXT, " +
            DBContract.CreditCard.COLUMN_USER_ID + " TEXT)";

    public static final String SQL_CREATE_DEBIT_CARD_TABLE = "CREATE TABLE " + DBContract.DebitCard.TABLE_NAME +
            " (" + DBContract.CreditCard._ID + " INTEGER PRIMARY KEY," +
            DBContract.DebitCard.COLUMN_CARD_NUMBER + " TEXT UNIQUE, " +
            DBContract.DebitCard.COLUMN_CARD_NAME + " TEXT, " +
            DBContract.DebitCard.COLUMN_CVV + " TEXT, " +
            DBContract.DebitCard.COLUMN_EXPIRY_DATE + " TEXT, " +
            DBContract.DebitCard.COLUMN_USER_ID + " TEXT)";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_USER_TABLE);
        db.execSQL(SQL_CREATE_PAN_CARD_TABLE);
        db.execSQL(SQL_CREATE_AADHAR_CARD_TABLE);
        db.execSQL(SQL_CREATE_CREDIT_CARD_TABLE);
        db.execSQL(SQL_CREATE_DEBIT_CARD_TABLE);
    }

    public User getUser(String email) {
        User user = null;
        String selection = DBContract.User.COLUMN_EMAIL + " = ?";
        String[] selectionArgs = new String[]{email};
            Cursor c = this.getReadableDatabase().query(DBContract.User.TABLE_NAME, DBContract.User.ALL_COLUMNS, selection, selectionArgs, null, null, null);
            if (c.moveToFirst()) {
                user = new User();
                user.email = c.getString(c.getColumnIndex(DBContract.User.COLUMN_EMAIL));
                user.id = c.getString(c.getColumnIndex(DBContract.User._ID));
                user.firstName = c.getString(c.getColumnIndex(DBContract.User.COLUMN_FIRST_NAME));
                user.password = c.getString(c.getColumnIndex(DBContract.User.COLUMN_PASSWORD));
                user.lastName = c.getString(c.getColumnIndex(DBContract.User.COLUMN_LAST_NAME));
                Log.d("DB", " Fetched user with id -> " + user.id);
            }

        return user;
    }

    public boolean saveUser(User user)  throws SQLiteConstraintException {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.User.COLUMN_EMAIL, user.email);
        cv.put(DBContract.User.COLUMN_FIRST_NAME, user.firstName);
        cv.put(DBContract.User.COLUMN_LAST_NAME, user.lastName);
        cv.put(DBContract.User.COLUMN_PASSWORD, user.password);
        SQLiteDatabase db = this.getReadableDatabase();
        long id = db.insertOrThrow(DBContract.User.TABLE_NAME, null, cv);
        db.close();
        return id > 0;
    };

    public boolean savePanCard(PanCard pCard)  throws SQLiteConstraintException {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.PanCard.COLUMN_DOB, pCard.dob);
        cv.put(DBContract.PanCard.COLUMN_NAME, pCard.fullName);
        cv.put(DBContract.PanCard.COLUMN_PAN_NUMBER, pCard.panNumber);
        cv.put(DBContract.PanCard.COLUMN_USER_ID, pCard.userId);
        SQLiteDatabase db = this.getReadableDatabase();
        long rows = db.insertOrThrow(DBContract.PanCard.TABLE_NAME, null, cv);
        db.close();
        return rows > 0;
    }

    public boolean saveAadharCard(AadharCard aCard)  throws SQLiteConstraintException {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.AadharCard.COLUMN_AADHAR_NUMBER, aCard.aadharNum);
        cv.put(DBContract.AadharCard.COLUMN_ADDRESS, aCard.address);
        cv.put(DBContract.AadharCard.COLUMN_FULL_NAME, aCard.fullName);
        cv.put(DBContract.AadharCard.COLUMN_USER_ID, aCard.userID);
        SQLiteDatabase db = this.getReadableDatabase();
        long row = db.insertOrThrow(DBContract.AadharCard.TABLE_NAME, null, cv);
        db.close();
        return row > 0;
    }

    public boolean deleteAadharCard(String aadharNum, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = new String[]{aadharNum, userId};
        Log.d("DB", "deleting aadhard card: Num  -> " + aadharNum + " userId -> " + userId);
        long row = db.delete(DBContract.AadharCard.TABLE_NAME, DBContract.AadharCard.COLUMN_AADHAR_NUMBER + " = ? AND " + DBContract.AadharCard.COLUMN_USER_ID + " = ?", whereArgs);
        db.close();
        return row > 0;
    }

    public ArrayList<AadharCard> getAllAahdarCards(String userId) {
        ArrayList<AadharCard> aCards = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = DBContract.AadharCard.COLUMN_USER_ID + " = ?";
        String[] selectionArgs = new String[]{userId};
        Cursor c = db.query(DBContract.AadharCard.TABLE_NAME, DBContract.AadharCard.ALL_COLUMNS, selection, selectionArgs, null, null, null);
        while (c.moveToNext()) {
            AadharCard aC = new AadharCard(
                    c.getString(c.getColumnIndex(DBContract.AadharCard.COLUMN_USER_ID)),
                    c.getString(c.getColumnIndex(DBContract.AadharCard.COLUMN_FULL_NAME)),
                    c.getString(c.getColumnIndex(DBContract.AadharCard.COLUMN_ADDRESS)),
                    c.getString(c.getColumnIndex(DBContract.AadharCard.COLUMN_AADHAR_NUMBER))
            );
            Log.d("DB", "fetched aadhar card: Num -> " + aC.aadharNum + " userId -> " + aC.userID);
            aCards.add(aC);
        }
        db.close();
        return aCards;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public boolean deletePanCard(String panNumber, String userId) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = new String[]{panNumber, userId};
        boolean result = db.delete(DBContract.PanCard.TABLE_NAME, DBContract.PanCard.COLUMN_PAN_NUMBER + " = ? AND " + DBContract.PanCard.COLUMN_USER_ID + " = ?", whereArgs) > 0;
        db.close();
        return result;
    }

    public ArrayList<PanCard> getAllPanCards(String userId) {
        ArrayList<PanCard> pCards = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = DBContract.PanCard.COLUMN_USER_ID + " = ?";
        String[] selectionArgs = new String[]{userId};
        Cursor c = db.query(DBContract.PanCard.TABLE_NAME, DBContract.PanCard.ALL_COLUMNS, selection, selectionArgs, null, null, null);
        while (c.moveToNext()) {
            PanCard pC = new PanCard(
                    c.getString(c.getColumnIndex(DBContract.PanCard.COLUMN_USER_ID)),
                    c.getString(c.getColumnIndex(DBContract.PanCard.COLUMN_NAME)),
                    c.getString(c.getColumnIndex(DBContract.PanCard.COLUMN_DOB)),
                    c.getString(c.getColumnIndex(DBContract.PanCard.COLUMN_PAN_NUMBER))
            );
            pCards.add(pC);
        }
        db.close();
        return pCards;
    }

    public boolean deleteBaseCard(String cardNum, String userId, String tableName) {
        SQLiteDatabase db = this.getWritableDatabase();
        String[] whereArgs = new String[]{cardNum, userId};
        boolean result = db.delete(tableName, DBContract.BaseCard.COLUMN_CARD_NUMBER + " = ? AND " + DBContract.BaseCard.COLUMN_USER_ID + " = ?", whereArgs) > 0;
        db.close();
        return result;
    }

    public boolean saveBaseCard(BaseCard baseCard, String tableName)  throws SQLiteConstraintException {
        ContentValues cv = new ContentValues();
        cv.put(DBContract.BaseCard.COLUMN_CARD_NAME, baseCard.fullName);
        cv.put(DBContract.BaseCard.COLUMN_CARD_NUMBER, baseCard.cardNum);
        cv.put(DBContract.BaseCard.COLUMN_CVV, baseCard.ccv);
        cv.put(DBContract.BaseCard.COLUMN_EXPIRY_DATE, baseCard.expiryDate);
        cv.put(DBContract.BaseCard.COLUMN_USER_ID, baseCard.userId);
        SQLiteDatabase db = this.getReadableDatabase();
        long row = db.insertOrThrow(tableName, null, cv);
        db.close();
        return row > 0;
    }

    public ArrayList<BaseCard> getAllBaseCards(String tableName, String userId) {
        ArrayList<BaseCard> bCards = new ArrayList<>();
        SQLiteDatabase db = this.getReadableDatabase();
        String selection = DBContract.BaseCard.COLUMN_USER_ID + " = ?";
        String[] selectionArgs = new String[]{userId};
        Cursor c = db.query(tableName, DBContract.DebitCard.ALL_COLUMNS, selection, selectionArgs, null, null, null);
        while (c.moveToNext()) {
            BaseCard bC = new BaseCard(
                    c.getString(c.getColumnIndex(DBContract.BaseCard.COLUMN_USER_ID)),
                    c.getString(c.getColumnIndex(DBContract.BaseCard.COLUMN_CARD_NAME)),
                    c.getString(c.getColumnIndex(DBContract.BaseCard.COLUMN_CVV)),
                    c.getString(c.getColumnIndex(DBContract.BaseCard.COLUMN_EXPIRY_DATE)),
                    c.getString(c.getColumnIndex(DBContract.BaseCard.COLUMN_CARD_NUMBER))
            );
            bCards.add(bC);
        }
        db.close();
        return bCards;
    }
}
