package com.kongent.cardtest;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class loginregister extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loginregister);
    }

    public void logins(View view){
        Intent i = new Intent(this,login.class);
        startActivity(i);
    }

        public void registers(View view){
        Intent i = new Intent(this,register.class);
        startActivity(i);
        finish();
    }

}
