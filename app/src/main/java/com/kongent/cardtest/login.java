package com.kongent.cardtest;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import com.kongent.cardtest.model.User;

public class login extends AppCompatActivity {
    private Snackbar snackBar;
    private EditText etEmail, etPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etEmail = findViewById(R.id.et_email);
        etPassword = findViewById(R.id.et_password);
        btnLogin = findViewById(R.id.btn_login);
    }

    public void login(View view){
        // validate required fields
        boolean hasErrors = false;
        User user = new User();
        user.email = etEmail.getText().toString();
        user.password = etPassword.getText().toString();
        if (TextUtils.isEmpty(user.email)) {
            etEmail.setError("Email is required.");
            hasErrors = true;
        }
        if (TextUtils.isEmpty(user.password)) {
            etPassword.setError("Password is required.");
            hasErrors = true;
        }

        if (!hasErrors) {
            new TaskLogin().execute(user);
        }
    }

    public void register(View view) {
        Intent i = new Intent(this, register.class);
        startActivity(i);
        finish();
    }

    private class TaskLogin extends AsyncTask<User, Void, User> {

        @Override
        protected void onPreExecute() {
            btnLogin.setEnabled(false);
            snackBar = Snackbar.make(findViewById(android.R.id.content), "Processing login...", Snackbar.LENGTH_INDEFINITE);
            snackBar.show();
        }

        @Override
        protected User doInBackground(User[] users) {
            DBHelper db = new DBHelper(getApplicationContext());
            User user = db.getUser(users[0].email);
            if (null != user && user.password.equals(users[0].password)) {
                return user;
            }
            return null;
        }

        @Override
        protected void onPostExecute(User user) {
            snackBar.dismiss();
            btnLogin.setEnabled(true);
            if (null != user) {
                Snackbar.make(findViewById(android.R.id.content), "Welcome " + user.firstName, Snackbar.LENGTH_SHORT).show();
                Intent intent = new Intent(login.this,showoradd.class);
                intent.putExtra(showoradd.ARG_USER_ID, user.id);
                startActivity(intent);
            } else {
                // failed to validate user, show message
                Snackbar.make(findViewById(android.R.id.content), "Invalid login credentials. Try again.", Snackbar.LENGTH_SHORT).show();
            }
        }
    }


}
