package com.kongent.cardtest;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.kongent.cardtest.model.AadharCard;
import com.kongent.cardtest.model.BaseCard;
import com.kongent.cardtest.model.PanCard;

import java.util.ArrayList;

public class ActivityViewCards extends AppCompatActivity {
    private int cardType;
    private String userId;
    private ListView listView;
    private ArrayList<AadharCard> aadharCards;
    private ArrayList<BaseCard> baseCards;
    private ArrayList<PanCard> panCards;
    private DBHelper db;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_cards);
        userId = getIntent().getExtras().getString(showoradd.ARG_USER_ID);
        cardType = getIntent().getExtras().getInt(showoradd.CARD_TYPE);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitleTextColor(getResources().getColor(android.R.color.white));
        setToolBarTitle(toolbar);
        setSupportActionBar(toolbar);
        listView = (ListView)findViewById(R.id.list_view);
        listView.setAdapter(new ListAdapter(this, 0));
        db = new DBHelper(this);
    }

    @Override
    public void onResume(){
        super.onResume();
        updateListItems();
    }

    private void setToolBarTitle(Toolbar t) {
        String title = "My Cards";
        Log.d("card", "card type -> " + cardType);
        switch (cardType) {
            case showoradd.ARG_CARD_TYPE_AADHAR: {
                title = "Aadhar Cards";
                break;
            }
            case showoradd.ARG_CARD_TYPE_CREDIT: {
                title = "Credit Cards";
                break;
            }
            case showoradd.ARG_CARD_TYPE_DEBIT: {
                title = "Debit Cards";
                break;
            }
            case showoradd.ARG_CARD_TYPE_PAN: {
                title = "Pan Cards";
                break;
            }
        }
        t.setTitle(title);
    }

    private class ListAdapter extends ArrayAdapter<String> {

        public ListAdapter(@NonNull Context context, int resource) {
            super(context, resource);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Log.d("Hey", "getView()");
            switch (cardType) {
                case showoradd.ARG_CARD_TYPE_AADHAR: {
                    AadharCard aCard = aadharCards.get(position);
                    AadharViewHolder aViewHolder = null;
                    if (null == convertView) {
                        aViewHolder = new AadharViewHolder();
                        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_aadhar_card, parent, false);
                        convertView.setTag(aViewHolder);
                        setupAadharItem(aCard, convertView, aViewHolder);
                    } else {
                        aViewHolder = (AadharViewHolder)convertView.getTag();
                        // ToDo: remember to remove this!
                        // view.findViewById(R.id.btn_delete).setOnClickListener(null);
                        setupAadharItem(aCard, convertView, aViewHolder);
                    }
                    break;
                }
                case showoradd.ARG_CARD_TYPE_CREDIT:
                case showoradd.ARG_CARD_TYPE_DEBIT: {
                    BaseCard bCard = baseCards.get(position);
                    BaseCardHolder bHolder = null;
                    if (null == convertView) {
                        bHolder = new BaseCardHolder();
                        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_base_card, parent, false);
                        convertView.setTag(bHolder);
                        setupBaseCardItem(bCard, convertView, bHolder);
                    } else {
                        bHolder = (BaseCardHolder) convertView.getTag();
                        setupBaseCardItem(bCard, convertView, bHolder);
                    }
                    break;
                }
                case showoradd.ARG_CARD_TYPE_PAN: {
                    PanCard pCard = panCards.get(position);
                    PanViewHolder pViewHolder = null;
                    if (null == convertView) {
                        pViewHolder = new PanViewHolder();
                        convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_pan_card, parent, false);
                        convertView.setTag(pViewHolder);
                        setupPanCardItem(pCard, convertView, pViewHolder);
                    } else {
                        pViewHolder = (PanViewHolder)convertView.getTag();
                        setupPanCardItem(pCard, convertView, pViewHolder);
                    }
                    break;
                }
            }
            return convertView;
        }

        @Override
        public int getCount() {
            Log.d("Hey", "getCount()");
            switch (cardType) {
                case showoradd.ARG_CARD_TYPE_AADHAR: {
                    return aadharCards == null ? 0 : aadharCards.size();
                }
                case showoradd.ARG_CARD_TYPE_CREDIT:
                case showoradd.ARG_CARD_TYPE_DEBIT: {
                    return baseCards == null ? 0 : baseCards.size();
                }
                case showoradd.ARG_CARD_TYPE_PAN: {
                   return panCards == null ? 0 : panCards.size();
                }
                default:{
                    return 0;
                }
            }
        }

    }

    private static class AadharViewHolder {
        TextView tvFullName;
        TextView tvAadharNum;
        TextView tvAadharAddress;
        Button btnDelete;
    }

    private static class PanViewHolder {
        TextView tvFullName;
        TextView tvDOB;
        TextView tvPanNumber;
        Button btnDelete;
    }

    private static class BaseCardHolder {
        TextView tvFullName;
        TextView tvCardNum;
        TextView tvExpiryDate;
        TextView tvCCV;
        Button btnDelete;
    }

    private void setupBaseCardItem(final BaseCard bCard, View convertView, BaseCardHolder bHolder) {
        bHolder.tvFullName = ((TextView)convertView.findViewById(R.id.tv_fullname));
        bHolder.tvCardNum = ((TextView)convertView.findViewById(R.id.tv_card_number));
        bHolder.tvExpiryDate = ((TextView)convertView.findViewById(R.id.tv_expiry_date));
        bHolder.tvCCV = ((TextView)convertView.findViewById(R.id.tv_cvv));
        bHolder.btnDelete = ((Button)convertView.findViewById(R.id.btn_delete));

        bHolder.tvFullName.setText(bCard.fullName);
        bHolder.tvCardNum.setText(bCard.cardNum);
        bHolder.tvExpiryDate.setText(bCard.expiryDate);
        bHolder.tvCCV.setText(bCard.ccv);
        bHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String tableName = cardType == showoradd.ARG_CARD_TYPE_CREDIT ? DBContract.CreditCard.TABLE_NAME : DBContract.DebitCard.TABLE_NAME;
                if (db.deleteBaseCard(bCard.cardNum, userId, tableName)) {
                    updateListItems();
                    Snackbar.make(findViewById(android.R.id.content), "Card successfully deleted!", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Failed to delete card", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setupPanCardItem(final PanCard pCard, View convertView, PanViewHolder pHolder) {
        pHolder.tvFullName = ((TextView)convertView.findViewById(R.id.tv_fullname));
        pHolder.tvDOB = ((TextView)convertView.findViewById(R.id.tv_dob));
        pHolder.tvPanNumber = ((TextView)convertView.findViewById(R.id.tv_pan_number));
        pHolder.btnDelete = ((Button)convertView.findViewById(R.id.btn_delete));

        pHolder.tvFullName.setText(pCard.fullName);
        pHolder.tvPanNumber.setText(pCard.panNumber);
        pHolder.tvDOB.setText(pCard.dob);
        pHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (db.deletePanCard(pCard.panNumber, userId)) {
                    updateListItems();
                    Snackbar.make(findViewById(android.R.id.content), "Card successfully deleted!", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Failed to delete card", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void setupAadharItem(final AadharCard aCard, View convertView, AadharViewHolder aHolder) {
        aHolder.tvAadharAddress = ((TextView)convertView.findViewById(R.id.tv_address));
        aHolder.tvAadharNum = ((TextView)convertView.findViewById(R.id.tv_aadhar_num));
        aHolder.tvFullName = ((TextView)convertView.findViewById(R.id.tv_fullname));
        aHolder.btnDelete = ((Button)convertView.findViewById(R.id.btn_delete));

        aHolder.tvAadharAddress.setText(aCard.address);
        aHolder.tvAadharNum.setText(aCard.aadharNum);
        aHolder.tvFullName.setText(aCard.fullName);
        aHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (db.deleteAadharCard(aCard.aadharNum, userId)) {
                    updateListItems();
                    Snackbar.make(findViewById(android.R.id.content), "Card successfully deleted!", Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(findViewById(android.R.id.content), "Failed to delete card", Snackbar.LENGTH_SHORT).show();
                }
            }
        });
    }
    private void updateListItems() {
        switch (cardType) {
            case showoradd.ARG_CARD_TYPE_AADHAR: {
                aadharCards = db.getAllAahdarCards(userId);
                break;
            }
            case showoradd.ARG_CARD_TYPE_CREDIT:
            case showoradd.ARG_CARD_TYPE_DEBIT: {
                String tableName = cardType == showoradd.ARG_CARD_TYPE_CREDIT ? DBContract.CreditCard.TABLE_NAME : DBContract.DebitCard.TABLE_NAME;
                baseCards = db.getAllBaseCards(tableName, userId);
                break;
            }
            case showoradd.ARG_CARD_TYPE_PAN: {
                panCards = db.getAllPanCards(userId);
                break;
            }
        }
        ((BaseAdapter)listView.getAdapter()).notifyDataSetChanged();
    }

}
