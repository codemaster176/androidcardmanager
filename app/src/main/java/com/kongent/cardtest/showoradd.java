package com.kongent.cardtest;

import android.app.Dialog;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.content.Intent;
import android.widget.TextView;

public class showoradd extends AppCompatActivity implements View.OnClickListener {
    public static final String ARG_USER_ID = "user_id";
    private String userId;
    private Dialog dialog;
    private static final int ACTION_ADD_CARD = 0;
    private static final int ACTION_VIEW_CARD = 1;
    public static final int ARG_CARD_TYPE_AADHAR = 2;
    public static final int ARG_CARD_TYPE_CREDIT = 3;
    public static final int ARG_CARD_TYPE_DEBIT = 4;
    public static final int ARG_CARD_TYPE_PAN = 5;
    public static final String CARD_TYPE = "card_type";
    private int actionType;
    private TextView tvDialogHeader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_showoradd);
        userId = getIntent().getExtras().getString(ARG_USER_ID);
        prepareDialog();
    }

    public void addCard(View view){
        tvDialogHeader.setText("SELECT CARD TYPE TO ADD");
        actionType = ACTION_ADD_CARD;
        dialog.show();
    }

    public void viewCards(View view){
        actionType = ACTION_VIEW_CARD;
        tvDialogHeader.setText("SELECT CARD TYPES TO VIEW");
        dialog.show();
    }

    private void prepareDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = LayoutInflater.from(this).inflate(R.layout.card_picker_dialog, null);
        view.findViewById(R.id.btn_aadhar).setOnClickListener(this);
        view.findViewById(R.id.btn_credit).setOnClickListener(this);
        view.findViewById(R.id.btn_debit).setOnClickListener(this);
        view.findViewById(R.id.btn_pan).setOnClickListener(this);
        view.findViewById(R.id.btn_exit).setOnClickListener(this);
        tvDialogHeader = (TextView)view.findViewById(R.id.tv_header);
        builder.setView(view);
        dialog = builder.create();
        dialog.setCancelable(false);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_aadhar: {
                if (actionType == ACTION_ADD_CARD) {
                    Intent i = new Intent(this, ActivityAddCard.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_AADHAR);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                if (actionType == ACTION_VIEW_CARD) {
                    Intent i = new Intent(this, ActivityViewCards.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_AADHAR);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                break;
            }
            case R.id.btn_credit: {
                if (actionType == ACTION_ADD_CARD) {
                    Intent i = new Intent(this, ActivityAddCard.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_CREDIT);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                if (actionType == ACTION_VIEW_CARD) {
                    Intent i = new Intent(this, ActivityViewCards.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_CREDIT);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                break;
            }
            case R.id.btn_debit: {
                if (actionType == ACTION_ADD_CARD) {
                    Intent i = new Intent(this, ActivityAddCard.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_DEBIT);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                if (actionType == ACTION_VIEW_CARD) {
                    Intent i = new Intent(this, ActivityViewCards.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_DEBIT);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                break;
            }
            case R.id.btn_pan: {
                if (actionType == ACTION_ADD_CARD) {
                    Intent i = new Intent(this, ActivityAddCard.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_PAN);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                if (actionType == ACTION_VIEW_CARD) {
                    Intent i = new Intent(this, ActivityViewCards.class);
                    i.putExtra(CARD_TYPE, ARG_CARD_TYPE_PAN);
                    i.putExtra(ARG_USER_ID, userId);
                    startActivity(i);
                }
                break;
            }
        }
        dialog.dismiss();
    }
}
