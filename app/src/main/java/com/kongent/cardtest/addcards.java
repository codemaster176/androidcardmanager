package com.kongent.cardtest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class addcards extends Activity {

    String cards[]={"DEBIT CARD","CREDIT CARD","PAN CARD","AADHAR CARD"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addcards);

        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                R.layout.activity_addcards, cards);

        ListView listView = (ListView) findViewById(R.id.card_list);
        listView.setAdapter(adapter);
    }
}
